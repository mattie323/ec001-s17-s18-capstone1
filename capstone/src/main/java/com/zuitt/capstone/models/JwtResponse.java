package com.zuitt.capstone.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {

    private static final long serialVersionUID = -496941886187655134L;

    private final String jwtToken;

    public JwtResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    // Getters and Setters
    public String getToken() {
        return this.jwtToken;
    }

}
