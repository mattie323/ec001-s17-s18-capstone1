package com.zuitt.capstone.repositories;
import com.zuitt.capstone.models.Course;
import org.springframework.data.repository.CrudRepository;

public interface CourseRepository extends CrudRepository<Course,Object> {
}


