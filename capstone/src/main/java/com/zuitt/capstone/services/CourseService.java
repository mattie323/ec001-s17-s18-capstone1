package com.zuitt.capstone.services;

import com.zuitt.capstone.models.Course;
import org.springframework.http.ResponseEntity;

public interface CourseService {
    void createCourse(String stringToken, Course course);
    // Viewing all posts
    Iterable<Course> getPosts();
    // Delete a post
    ResponseEntity deletePost(String stringToken, Long id);

    //Update a post
    ResponseEntity updatePost(Long id,String stringToken, Course course);


//  to get all post of a specific user


}
